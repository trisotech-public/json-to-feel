# JSON to FEEL converter

Simple online utility that allows to convert JSON files in FEEL (from the OMG DMN standard) literal expression.

This is accessible [online](https://json-to-feel-trisotech-public-f9ff81889ba95f02ce47794837d8f3e08.gitlab.io/)